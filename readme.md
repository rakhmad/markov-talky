about
==
Shitty markov chain bot. Most of the code is snagged off the net.

usage
==

Compile from source and pull a version of TTYTTer from [Floodgap](http://floodgap.com/software/ttytter/). Make sure it is named `ttytter.pl` and grab some text from the net. RFCs, books and large blocks work best. Put this in a plain ASCII file called `corpus.txt`.

Run ttytter with the options `-oauthwizard -keyf=_markov` and follow getting TTYtter set up for usage with your twitter account. I used the keyfile `~/.ttytterkey_markov` because I use the box it lives on. Don't want to do this? Just mangle the appropriate line in `Program.cs`. 

Build, and invoke `markov.exe` -- it will automatically start tweeting. Add the `-d` option to see it in fast-forward and not pipe to TTYTter. In Debug mode, it generates a tweet once every 5 seconds, as opposed to the 30m-3hr it usually does.

This depends on the following things:

  * A block of text named `corpus.txt`
  * TTYtter as `ttytter.pl`
  * Mono of some recent vintage (tested primarily on 2.10/ArchLinux). 

If you can't figure it out, don't worry -- you're not the only one.

misc.
==
See it in action: [@hacking_ebooks](https://twitter.com/hacking_ebooks).
