﻿/*
 * User: indrora
 * Date: 10/16/2012
 * Time: 10:35 AM
 * 
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace markov
{
	class Program
	{
		private const string corpus_raw_name = "corpus.txt";
		
		public static void Main(string[] args)
		{
			bool debug = false;
			if(args.Length > 0 && args[0] == "-d")
				debug = true;
			Console.WriteLine("[!] Generate Chain");	
			Chain<String> markov_chain = null;
			markov_chain = crank_corpus();
			
			Console.WriteLine("[!] Set up formats");
			Dictionary<double,Func<String>> formats = new Dictionary<double, Func<String>>();

			// Keys = our weight.

			formats.Add(0.22, ()=> getText(markov_chain, 100));
			formats.Add(0.12, ()=> getText(markov_chain, 130));
			formats.Add(0.30, ()=> {
				 string part1 = getText(markov_chain, 20);
				 string part2 = getText(markov_chain, 30);
				 string part3 = getText(markov_chain, 20);
				 return String.Format("{0}; {1}; {2}", part1.Trim(),part2.Trim(),part3.Trim()  );
			} );
			formats.Add(0.80, ()=>getText(markov_chain, 50));
			formats.Add(0.01, ()=>getText(markov_chain, 20));
			formats.Add(0.05, ()=>getText(markov_chain, 90));
	
			bool run = true;
			
			
			Console.WriteLine("[!] Start process");
			System.IO.TextWriter tw_out;
			Process twitter_proc = new Process();
			if(debug)
				tw_out = Console.Out;
			else
			{
				twitter_proc.StartInfo.UseShellExecute = false;
				twitter_proc.StartInfo.FileName = "./ttytter.pl";
				twitter_proc.StartInfo.Arguments = "-script -keyf=_markov";
				twitter_proc.StartInfo.RedirectStandardInput = true;
				twitter_proc.Start(); 
				tw_out = twitter_proc.StandardInput;
			}

			int num_tweets = 0;
			Console.CancelKeyPress += delegate { Console.WriteLine("[!] Exiting...");  run = false; };
			while(run)
			{
				Func<String> func = (formats.RandomElementByWeight(e=>((float)e.Key))).Value;
				String line = (func().Trim());
				tw_out.WriteLine(line);
				TimeSpan sleepspan = new TimeSpan(0,r.Next(30,300),0);
				if(debug)
					sleepspan= new TimeSpan(0,0,5);
				Console.WriteLine("{0} tweets, next in {1}", ++num_tweets, sleepspan.ToString());
				System.Threading.Thread.Sleep(sleepspan);
				
			}

			// Close the debug line if we're not using it.
			if(!debug) twitter_proc.Close();	
		}

			static Random r = new Random();	
		private static string getText(Chain<String> chain, int maxLen)
		{

			StringBuilder bx;
			var result = new List<String>(chain.Generate(r.Next(3,50)));
			bx = new StringBuilder();
			foreach(String item in result)			
				if(bx.ToString().Length < maxLen) bx.Append(item);
				else
					break;
			return bx.ToString();
		}

		private static IEnumerable<string> Split(string subject)
		{
			List<string> tokens = new List<string>();
			Regex regex = new Regex(@"(\W+)");
			tokens.AddRange(regex.Split(subject));

			return tokens;
		}
		private static string Tidy(string p)
		{
			string Disallowed = "\n\t\r[]().,<>;%^*#@-";
			string result = p;
			foreach(char x in Disallowed.ToCharArray()) result = result.Replace(x, ' ');
			string compress = result;
			
			do
			{
				result = compress;
				compress = result.Replace("  ", " ");
			}
			while (result != compress);

			return result;
		}
		
		public static Chain<String> crank_corpus()
		{
			String essay= System.IO.File.ReadAllText(corpus_raw_name);
			String tidied_essay = Tidy(essay);
			List<String> seed = new List<string>( Split(tidied_essay) );
			
			Chain<String> corpus = new Chain<string>(seed,4);
			return corpus;
		}
	}

}
